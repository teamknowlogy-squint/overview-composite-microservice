package com.teamknowlogy.squint.service;

import com.teamknowlogy.squint.event.RequestOverviewEvent;
import com.teamknowlogy.squint.event.ResponseOverviewEvent;

import java.util.concurrent.Future;

/**
 * Created by ricardo on 02/08/17.
 */
public interface OverviewCompositeService {
    Future<ResponseOverviewEvent> getCompositeOverview(final RequestOverviewEvent event);
}
