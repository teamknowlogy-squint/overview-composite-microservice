package com.teamknowlogy.squint.service.impl;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.command.AsyncResult;
import com.teamknowlogy.squint.client.*;
import com.teamknowlogy.squint.domain.Overview;
import com.teamknowlogy.squint.event.RequestOverviewEvent;
import com.teamknowlogy.squint.event.ResponseOverviewEvent;
import com.teamknowlogy.squint.service.OverviewCompositeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * Created by ricardo on 02/08/17.
 */
@Service
public class OverviewCompositeServiceImpl implements OverviewCompositeService {

  @Autowired
  private FeignArticleClient feignArticleClient;

  @Autowired
  private FeignTweetClient feignTweetClient;

  @Autowired
  private FeignVideoClient feignVideoClient;

  @Autowired
  private FeignPostClient feignPostClient;

  @Autowired
  private FeignPinClient feignPinClient;

  @Autowired
  private FeignSearchClient feignSearchClient;

  @Autowired
  private FeignAppClient feignAppClient;

  private static final ForkJoinPool forkJoinPool = new ForkJoinPool(7);

  @Async
  @Override
  @SuppressWarnings({"unchecked"})
  @HystrixCommand(
    groupKey = "squint-overview-composite",
    fallbackMethod = "defaultGetCompositeOverview",
    commandKey = "OverviewCompositeServiceImpl#getCompositeOverview"
  )
  public Future<ResponseOverviewEvent> getCompositeOverview(final RequestOverviewEvent event) {

    final List<Callable<String>> callableList = Arrays.asList(
      () -> this.feignArticleClient.findTrends(event.getYt()),
      () -> this.feignSearchClient.findTrends(event.getGo()),
      () -> this.feignTweetClient.findTrends(event.getTw()),
      () -> this.feignVideoClient.findTrends(event.getYt()),
      () -> this.feignPostClient.findTrends(event.getIg()),
      () -> this.feignAppClient.findTrends(event.getYt().toLowerCase()),
      () -> this.feignPinClient.findTrends()
    );

    final List<String> responseList = forkJoinPool
      .invokeAll(callableList)
      .stream()
      .map(future -> {
        try {
          return future.get();
        } catch (final Exception e) {
          return "[]";
        }
      })
      .collect(Collectors.toList());

    return new AsyncResult() {
      @Override
      public ResponseOverviewEvent invoke() {
        return ResponseOverviewEvent.builder()
          .overview(Overview.builder()
            .pins(responseList.get(6))
            .apps(responseList.get(5))
            .posts(responseList.get(4))
            .videos(responseList.get(3))
            .tweets(responseList.get(2))
            .searches(responseList.get(1))
            .articles(responseList.get(0))
            .build())
          .build();
      }
    };
  }

  @HystrixCommand
  @SuppressWarnings({"unchecked"})
  public Future<ResponseOverviewEvent> defaultGetCompositeOverview(final RequestOverviewEvent event) {
    return new AsyncResult() {
      @Override
      public ResponseOverviewEvent invoke() {
        return ResponseOverviewEvent.builder()
          .overview(Overview.builder()
            .pins("[]")
            .apps("[]")
            .posts("[]")
            .videos("[]")
            .tweets("[]")
            .searches("[]")
            .articles("[]")
            .build())
          .build();
      }
    };
  }
}
