package com.teamknowlogy.squint.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * Created by ricardo on 02/08/17.
 */
@FeignClient(value = "${feign.ms.article.id}")
public interface FeignArticleClient {

  @GetMapping("/countries/{countryId}/trends")
  String findTrends(@PathVariable("countryId") final String countryId);
}
