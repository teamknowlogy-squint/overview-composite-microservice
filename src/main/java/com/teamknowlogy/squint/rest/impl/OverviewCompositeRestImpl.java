package com.teamknowlogy.squint.rest.impl;

import com.teamknowlogy.squint.event.RequestOverviewEvent;
import com.teamknowlogy.squint.event.ResponseOverviewEvent;
import com.teamknowlogy.squint.rest.OverviewCompositeRest;
import com.teamknowlogy.squint.service.OverviewCompositeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by ricardo on 01/08/17.
 */
@RestController
@RequestMapping(path = "/overviews")
public class OverviewCompositeRestImpl implements OverviewCompositeRest {

    @Autowired
    private OverviewCompositeService overviewCompositeService;

    @Override
    @GetMapping
    @CrossOrigin
    public Mono<ResponseOverviewEvent> getOverviews(
            @RequestParam("yt") @NotNull @Size(max = 20) final String yt,
            @RequestParam("tw") @NotNull @Size(max = 10) final String tw,
            @RequestParam("go") @NotNull @Size(max = 20) final String go,
            @RequestParam("ig") @NotNull @Size(max = 20) final String ig
    ) {

        final RequestOverviewEvent event = RequestOverviewEvent
                .builder()
                .yt(yt).tw(tw).go(go).ig(ig)
                .build();

        return Mono.fromCallable(() -> overviewCompositeService.getCompositeOverview(event).get());
    }
}
