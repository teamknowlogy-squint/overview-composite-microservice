package com.teamknowlogy.squint.rest;

import com.teamknowlogy.squint.event.ResponseOverviewEvent;
import reactor.core.publisher.Mono;

/**
 * Created by ricardo on 01/08/17.
 */
public interface OverviewCompositeRest {
    Mono<ResponseOverviewEvent> getOverviews(
            final String yt,
            final String tw,
            final String go,
            final String ig
    );
}
