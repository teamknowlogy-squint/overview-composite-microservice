package com.teamknowlogy.squint.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by ricardo on 02/08/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Overview {

    private String pins;
    private String apps;
    private String posts;
    private String tweets;
    private String videos;
    private String articles;
    private String searches;
}
