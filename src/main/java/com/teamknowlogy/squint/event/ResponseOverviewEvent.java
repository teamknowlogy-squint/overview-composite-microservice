package com.teamknowlogy.squint.event;

import com.teamknowlogy.squint.domain.Overview;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ricardo on 02/08/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseOverviewEvent {
    private Overview overview;
}
