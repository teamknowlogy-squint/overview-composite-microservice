package com.teamknowlogy.squint.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ricardo on 02/08/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestOverviewEvent {

    private String tw;
    private String yt;
    private String go;
    private String ig;
}
