package com.teamknowlogy.squint.config;

import org.springframework.boot.web.reactive.context.ReactiveWebServerApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.EnableWebFlux;

/**
 * Created by ricardo on 02/08/17.
 */
@EnableWebFlux
@Configuration
public class RestConfig extends ReactiveWebServerApplicationContext {}
