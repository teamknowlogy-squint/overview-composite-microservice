package com.teamknowlogy.squint.config;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

/**
 * Created by ricardo on 02/08/17.
 */
@EnableCaching
@Configuration
public class CacheConfig {}
