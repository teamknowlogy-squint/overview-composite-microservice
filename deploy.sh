#!/bin/sh
# deploy.sh

if [[ "$#" == 3 ]]
  then

  CLOUD_IMAGE_ENV=$1
  CLOUD_KUBERNETES_ENV=$2
  CLOUD_IMAGE_NAME=$3

  docker build -t gcr.io/concise-foundry-173614/${CLOUD_IMAGE_NAME}:${CLOUD_IMAGE_ENV} .

  gcloud docker -- push gcr.io/concise-foundry-173614/${CLOUD_IMAGE_NAME}:${CLOUD_IMAGE_ENV}

  kubectl delete -f deployment-${CLOUD_IMAGE_ENV}.yaml --namespace=${CLOUD_KUBERNETES_ENV}

  kubectl create -f deployment-${CLOUD_IMAGE_ENV}.yaml --namespace=${CLOUD_KUBERNETES_ENV}

else
  echo "You should set arguments."
	exit 1
fi
