FROM java:8-jre-alpine
VOLUME /tmp
ADD target/squint-overview-composite-0.0.1.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]