# overview-composite-microservice
Squint Overview Composite to wrap social media services into one and expose it to public api

## Requirements

- JDK 1.8
- Apache Maven 3.x

## Stack

- JSE 8
- Spring Boot 2.x
- Spring Boot Actuator 2.x
- Spring Boot Undertow 2.x
- Spring Boot Test 2.x
- Spring Cloud Netflix Config Client 2.x
- Spring Cloud Netflix Eureka Client 2.x
- Spring Cloud Netflix Eureka Feign 2.x

## Plugins

- Lombok

## Contribution guide

### Remotes

The **remotes** follow the convention:

- _**origin**_: fork in the account of the developer

- _**upstream**_: main repository

### Commit Style

Please consider de following git styles for the commit messages:

http://udacity.github.io/git-styleguide/

### Building

For local environment:

```sh
$ mvn clean install -Dspring.cloud.config.profile=local -P local
```

For development environment:

```sh
$ mvn clean install -Dspring.cloud.config.profile=development -P development
```

For staging environment:

```sh
$ mvn clean install -Dspring.cloud.config.profile=staging -P staging
```

For production environment:

```sh
$ mvn clean install -Dspring.cloud.config.profile=production -P production
```

### Running

For local environment:

```sh
$ mvn clean spring-boot:run -Dspring.cloud.config.profile=local -P local
```

For development environment:

```sh
$ mvn clean spring-boot:run -Dspring.cloud.config.profile=development -P development
```

For staging environment:

```sh
$ mvn clean spring-boot:run -Dspring.cloud.config.profile=staging -P staging
```

For production environment:

```sh
$ mvn clean spring-boot:run -Dspring.cloud.config.profile=production -P production
```

You can use the new syntax: "-Dspring-boot.run.profiles=" instead of "-Dspring.profiles.active="

### Packaging

Using docker environment we have:

```sh
$ docker build -t DOCKER_ORCHESTRATOR/ORCHESTRATOR_ID/DOCKER_IMAGE_NAME:DOCKER_IMAGE_TAG .
```

Only for local environment:

```sh
$ docker build -t app:local .
```

Only for cloud environment and Google Container Registry orchestrator:8146

```sh
$ docker build -t gcr.io/GOOGLE_PROJECT_ID/DOCKER_IMAGE_NAME:DOCKER_IMAGE_TAG .
```

### Exposing

Only for local environment:

```sh
$ docker run -p 8080:8080 app
```

### Deploying

```sh
$ bash -x deploy.sh CLOUD_IMAGE_ENVIRONMENT KUBERNETES_ENVIRONMENT APPLICATION_NAME
```

## License

Teamknowlogy
